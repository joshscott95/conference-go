from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/={city}+{state}"

    try:
        response = requests.get(url, headers=headers)
        data = response.json()

        if data["results"]:
            return {"picture_url": data["results"][0]["urls"]["small"]}
    except Exception as e:
        print(f"Error fetching photo: {str(e)}")


def get_weather_data(city, state, country_code="US", limit=1):
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{country_code}&limit={limit}&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(geocode_url)
    geocode_data = response.json()

    lat = geocode_data[0]["lat"]
    lon = geocode_data[0]["lon"]

    weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url)
    weather_data = response.json()

    return {
        "temperature": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
