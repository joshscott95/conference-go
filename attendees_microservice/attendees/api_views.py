import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO
from django.views.decorators.http import require_http_methods


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]

    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        return {"has_account": count > 0}
        # return {
        #     "conference": {
        #         "name": o.conference.name,
        #         "href": f"https://example.com/conference/{o.conference.id}",
        #     }
        # }


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name"]

    def get_extra_data(self, o):
        return {"href": o.get_api_url()}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):

    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )

    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse({"error": "Attendee not found"}, status=404)

    if request.method == "GET":
        encoder = AttendeeDetailEncoder()
        attendee_data = encoder.default(attendee)
        return JsonResponse(attendee_data)
    elif request.method == "DELETE":
        attendee.delete()
        return JsonResponse({"message": "Attendee deleted successfully"})
    elif request.method == "PUT":
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON data"}, status=400)
        # Update the attendee object with the new data
        if "name" in data:
            attendee.name = data["name"]
        if "email" in data:
            attendee.email = data["email"]
        if "company_name" in data:
            attendee.company_name = data["company_name"]

        # Save the updated attendee object
        attendee.save()

        # Return a JSON response indicating success
        return JsonResponse({"message": "Attendee updated successfully"})
    else:
        return JsonResponse({"error": "Method not allowed"}, status=405)
